CC = gcc
FLAGS = -g -std=c99 -pedantic-errors -Wall -Werror -O2

all: scalar-product prime-checker

clean:
	rm scalar-product
	rm prime-checker
	
scalar-product: scalar-product.c
	$(CC) $(FLAGS) scalar-product.c -o scalar-product
prime-checker: prime-checker.c
	$(CC) $(FLAGS) prime-checker.c -o prime-checker
