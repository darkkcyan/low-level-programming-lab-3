#include <stdio.h>
#include <stdbool.h>
bool is_prime(unsigned long num) {
    if (num == 2) return true;
    if (num <= 1 || !(num & 1)) return false;
    unsigned long i, num_minus_sqr_i;
    // num_minus_sqr_i will store the value of (num - i * i) after every loop
    // below is an attempt to write a loop without sqrt, multiplication or division
    for (i = 3, num_minus_sqr_i = num - 1; num_minus_sqr_i >= (i - 1) << 2; i += 2) {
        // compare to (i - 1) * 4 because:
        //   i * i - (i - 2) * (i - 2) = i * i - (i * i - 4 * i + 4) = 4 * i - 4
        num_minus_sqr_i -= (i - 1) << 2;
        if (num % i) continue;
        return false;
    }
    return true;
}
int main(void) {
    unsigned long num;
    scanf("%lu", &num);
    printf("%s\n", is_prime(num) ? "yes" : "no");
    return 0;
}
