#include <stdio.h>
#include <stdbool.h>
#include <string.h>

long scalar_product(size_t size, int* a, int* b) {
    long ans = 0;
    for (; size--; ++a, ++b) {
        ans += *a * *b;
    }
    return ans;
}

void print_array(size_t size, int* a) {
    for (size_t i = 0; i < size; ++i) {
        printf("%d ", a[i]);
    }
}
void read_array(size_t size, int* a) {
    for (size_t i = 0; i < size; ++i) {
        scanf("%d", a + i);
    }
}
        
#define ARRAY_SIZE 100000
// defaults values for example from the book
size_t size = 3;
int a[ARRAY_SIZE] = {1, 2, 3}, b[ARRAY_SIZE] = {4, 5, 6};
int main(int argc, char** argv) {
    bool print_msg = true;
    bool use_example = false;
    for (; argc--; argv++) {
        if (strcmp(*argv, "-e") == 0) use_example = true;
        if (strcmp(*argv, "-s") == 0) print_msg = false;
    }
    if (use_example) print_msg = false;
    if (print_msg) {
        printf("First entered number (N) is the array size.\n");
        printf("Next entered N numbers will be elements of the first array.\n");
        printf("Then next entered N numbers will be elements of the second array.\n");
        printf("If you don't want this message, use flag -s\n");
        printf("If you want to see the an example, use flag -e\n");
    }
    if (use_example) {
        printf("Array size: %zu\n", size);
        printf("First array:\n");
        print_array(size, a);
        printf("\nSecond array:\n");
        print_array(size, b);
        printf("\nResult: ");
    } else {
        scanf("%zu", &size);
        read_array(size, a);
        read_array(size, b);
    }
    long ans = scalar_product(size, a, b);
    printf("%ld\n", ans);
    return 0;
}

